http://data.seoul.go.kr/openinf/sheetview.jsp?infId=OA-118
// 서울시 역코드로 지하철역 위치 조회

http://data.seoul.go.kr/openinf/fileview.jsp?infId=OA-12914
// 서울시 지하철호선별 역별 승하차 인원 정보

CREATE DATABASE vworld CHARACTER SET utf8 COLLATE utf8_general_ci;
//데이터베이스 생성
use vworld;
show tables;



CREATE TABLE subway_time (
  date VARCHAR(10),
  subway_no VARCHAR(20),
  subway_id INT(5),
  subway_name VARCHAR(50),
  subway_on INT(11),
  subway_out INT(11),
  update_date VARCHAR(10)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

load data local infile '/home/limmon/Desktop/Workspace/data/csv/201501.csv' into table subway_time character set UTF8 fields terminated by ',';

CREATE TABLE subway_code (
  subway_id INT(5),
  subway_name VARCHAR(50),
  wgs_x VARCHAR(20),
  wgs_y VARCHAR(20)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

load data local infile '/home/limmon/Desktop/Workspace/data/subway.csv' into table subway_code character set UTF8 fields terminated by ',';
