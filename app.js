var express = require('express');
var app = express();
var router = require('./router/main')(app); // 라우터 모듈을 ./router 폴더의 main.js 에서 불러옴

const path = require('path');
const fs = require('fs');

const port = 8070;

app.use(express.static('public'));

app.set('views', __dirname + '/views'); // HTML 위치 경로
app.set('view engine', 'ejs'); // 서버가 HTML 렌더링시 ejs로 렌더링 가능하게 함
app.engine('html', require('ejs').renderFile);

app.get('/sleep/:id', (req, res) => {
  let id = req.param('id');
  setTimeout(() => {
    res.send('OK');
  }, 3000);
  console.log(`sleep ${id}`);
});

const options = {
  key: fs.readFileSync(__dirname + '/server.key'),
  cert: fs.readFileSync(__dirname + '/server.crt'),
};

/*
spdy
  .createServer(options, app)
  .listen(port, (error) => {
    if (error) {
      console.error(error);
      return process.exit(1);
    }
    console.log(`Listening on port ${port}.`);
  });

*/
var server = app.listen(8070, function(){
    console.log("Express server has started on port 8070");
});
