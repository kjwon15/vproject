#!/usr/bin/env python
import csv
import sqlite3

from selenium import webdriver
from selenium.webdriver.support import ui
from selenium.webdriver.support import expected_conditions as EC

WAIT_SECONDS = 30
VENDORS = tuple('vgondk')
REPEAT = 5

conn = sqlite3.connect('out.sqlite')
conn.execute(
    'create table if not exists init_time('
    'browser, vendor, nocache, cache'
    ');'
)


def get_count(browser, vendor):
    count = conn.execute(
        'select count(*) from init_time where '
        'browser = ? and '
        'vendor = ?;',
        (browser, vendor)
    ).fetchone()[0]
    return count


def out_to_csv():
    with open('init_time.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator='\n')
        writer.writerow((
            'browser',
            'vendor',
            'wo/ cache',
            'w/ cache',
        ))

        cur = conn.execute(
            'select browser, vendor, nocache, cache '
            'from init_time '
            'order by browser, vendor'
        )

        for row in cur:
            writer.writerow(row)


def test(driver_cls, url):
    if driver_cls is webdriver.Ie:
        driver = driver_cls(capabilities={
            'ie.ensureCleanSession': True,
        })
    else:
        driver = driver_cls()
    wait = ui.WebDriverWait(driver, WAIT_SECONDS)
    # Non-Cache
    driver.get(url)
    wait.until(EC.title_contains('-init'))

    init_time_nocache = driver.execute_script('return window.initTime')

    driver.get(url)
    wait.until(EC.title_contains('-init'))
    init_time_cache = driver.execute_script('return window.initTime')

    driver.close()

    return init_time_nocache, init_time_cache


drivers = {
    'firefox': webdriver.Firefox,
    'chrome': webdriver.Chrome,
    'ie': webdriver.Ie,
}


def main():
    for driver_name, driver_cls in drivers.items():
        for vendor in VENDORS:
            count = get_count(driver_name, vendor)
            for try_count in range(REPEAT - count):
                url = 'http://localhost:8070/{}'.format(vendor)
                nocache, cache = test(driver_cls, url)
                print('{} {} {} {}'.format(
                    driver_name, vendor, nocache, cache))
                conn.execute(
                    'insert into init_time'
                    '(browser, vendor, nocache, cache) '
                    'values (?, ?, ?, ?)',
                    (driver_name, vendor, nocache, cache)
                )
                conn.commit()

    out_to_csv()


if __name__ == '__main__':
    main()
