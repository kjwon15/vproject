var pool = require('../db/db_con.js');
var bodyparser = require('body-parser');

module.exports = function(app, fs) {
    app.use(bodyparser.json());
    app.use(bodyparser.urlencoded({
        extended: false
    }));

    let cachedData = {};

    //브이월드
    app.get('/v', (req, res) =>
        res.render('main', {
            vendor: 'vworld',
        })
    );

    //구글
    app.get('/g', (req, res) =>
        res.render('main', {
            vendor: 'googlemap',
        })
    );

    app.get('/d', (req, res) =>
        res.render('main', {
            vendor: 'daummap',
        })
    );

    app.get('/n', (req, res) =>
        res.render('main', {
            vendor: 'navermap',
        })
    );

    app.get('/o', (req, res) =>
        res.render('main', {
            vendor: 'osm',
        })
    );

    app.get('/k', (req, res) =>
        res.render('main', {
            vendor: 'olleh',
        })
    );

    app.get('/date/:date', (req, response) => {
        const sql = "SELECT c.wgs_x, c.wgs_y, t.subway_on::int from subway_time t " +
      "JOIN subway_code c ON (t.subway_id=c.subway_id) " +
      "where t.date=$1 and c.wgs_x != '' and c.wgs_y != ''";
        let date = req.params.date.replace(/-/gi, '');
        if (date in cachedData) {
            console.log(`Use cached ${date}`);
            let rows = cachedData[date];
            response.send({rows: rows});
        } else {
            console.log(`date: ${date}`);
            pool.query(sql, [date])
                .then(res => {
                    cachedData[date] = res.rows;
                    response.send({rows: res.rows});
                })
                .catch(err => console.error(err.stack));
        }
    });

    app.get('/datecount/:count', (req, res) => {
        let count = parseInt(req.params.count);
        let sql = "SELECT date from subway_time group by date order by date limit $1::int";
        let promises = [];
        let combined = {};
        pool.query(sql, [count])
            .then(result => {
                for (let row of result.rows) {
                    let date = row.date;
                    if (date in cachedData) {
                        combined[date] = cachedData[date];
                    } else {
                        let promise = pool.query(
                            "select c.wgs_x, c.wgs_y, t.subway_on::int from subway_time as t " +
              "join subway_code as c on t.subway_id = c.subway_id " +
              "where t.date=$1 and c.wgs_x != '' and c.wgs_y != ''",
                            [date])
                            .then(result => {
                                cachedData[date] = result.rows;
                                combined[date] = result.rows;
                            });
                        promises.push(promise);
                    }
                }

                Promise.all(promises)
                    .then(() => {
                        res.send(combined);
                    });
            });
    });

    app.get('/count/:count', (req, res) => {
        let count = parseInt(req.params.count);
        let sql = "SELECT c.wgs_x, c.wgs_y, t.subway_on::int from subway_time t " +
      "JOIN subway_code c ON (t.subway_id=c.subway_id) " +
      "where c.wgs_x != '' and c.wgs_y != '' " +
      "limit $1";
        console.log(`count: ${count}`);
        pool.query(sql, [count])
            .then(result => {
                res.send({ rows: result.rows });
            })
            .catch(err => {
                console.error(err);
            });
    });

};
