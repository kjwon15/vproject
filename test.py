import csv
import itertools
import sqlite3
import sys
from selenium import webdriver
from selenium.webdriver.support import ui
from selenium.webdriver.support import expected_conditions as EC

WAIT_SECONDS = 60
VENDORS = tuple('vgon')
AMOUNTS = range(1000, 20000 + 1, 1000)
DATES = range(1, 100 + 1)
REPEAT = 5

drivers = {
    'chrome': webdriver.Chrome,
    'firefox': webdriver.Firefox,
    'ie': webdriver.Ie,
}

conn = sqlite3.connect('out.sqlite')
conn.execute(
    'create table if not exists results('
    'drawtype, browser, vendor, amount, idle, api, total'
    ');'
)

conn.execute(
    'create table if not exists timeline('
    'drawtype, browser, vendor, dates, '
    'amount, api, total'
    ');'
)


def get_count(mode, draw_type, browser, vendor, arg):
    if mode == 'normal':
        count = conn.execute(
            'select count(*) from results where '
            'drawtype = ? and '
            'browser = ? and '
            'vendor = ? and '
            'amount = ?;',
            (draw_type, browser, vendor, arg)
        ).fetchone()[0]
    elif mode == 'timeline':
        count = conn.execute(
            'select count(*) from timeline where '
            'drawtype = ? and '
            'browser = ? and '
            'vendor = ? and '
            'dates = ?;',
            (draw_type, browser, vendor, arg)
        ).fetchone()[0]
    return count


def out_to_csv():
    with open('out.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator='\n')
        writer.writerow((
            'browser',
            'vendor',
            'amount',
            'idle time',
            'api call time',
            'total time',
        ))

        cur = conn.execute(
            'select browser, vendor, amount, idle, api, total '
            'from results '
            'order by browser, vendor, amount'
        )

        for row in cur:
            writer.writerow(row)


def test_with_count(driver, url, amount, draw_type):
    wait = ui.WebDriverWait(driver, WAIT_SECONDS)
    driver.get(url)

    wait.until(EC.title_contains('-init'))

    init_time = driver.execute_script('return window.initTime')
    print('Idle time: {}'.format(init_time))

    driver.execute_script(
        'setTimeout(function(){{testWithCount({:d}, "{}")}}, 0)'.format(
            amount, draw_type))
    wait.until(EC.title_contains('-complete'))
    api_time = driver.execute_script('return window.apiCallTime')
    total_time = driver.execute_script('return window.totalTime')
    print(api_time, total_time)

    return init_time, api_time, total_time


def test_timeline(driver, url, dates, draw_type):
    wait = ui.WebDriverWait(driver, WAIT_SECONDS)
    driver.get(url)
    wait.until(EC.title_contains('-init'))
    init_time = driver.execute_script('return window.initTime')
    print('Idle time: {}'.format(init_time))

    driver.execute_script('testTimeline({:d}, "{}")'.format(
        dates, draw_type))
    wait.until(EC.title_contains('-timeline-complete'))

    result = driver.execute_script('return window.result')
    amount = result['amount']
    api_time = result['apiCallTime']
    total_time = result['totalTime']

    return amount, api_time, total_time


def normal_test(draw_type):
    for driver_name, driver_cls in drivers.items():
        try:
            driver = driver_cls()
        except Exception:
            print('Pass {}'.format(driver_name))
            continue

        print('Testing with {}'.format(driver_name))
        try:
            for amount, vendor in itertools.product(AMOUNTS, VENDORS):
                count = get_count('normal', draw_type, driver_name, vendor, amount)
                try:
                    for try_count in range(REPEAT - count):
                        print('Testing {} {} ({})'.format(
                            vendor, amount, try_count))
                        result = test_with_count(
                            driver,
                            'http://localhost:8070/{}'.format(vendor),
                            amount, draw_type)
                        conn.execute(
                            'insert into results'
                            '(drawtype, browser, vendor, amount, idle, api, total) '
                            'values(?, ?, ?, ?, ?, ?, ?)',
                            (draw_type, driver_name, vendor, amount, *result))
                        conn.commit()
                except Exception as e:
                    print(e)
                    try:
                        driver.close()
                    except Exception:
                        pass
                    finally:
                        driver = driver_cls()
                    continue
        finally:
            driver.close()


def timeline_test(draw_type):
    for driver_name, driver_cls in drivers.items():
        try:
            driver = driver_cls()
        except Exception:
            print(f'Pass {driver_name}')
        else:
            print(f'Testing with {driver_name}')

        try:
            for dates, vendor in itertools.product(DATES, VENDORS):
                count = get_count('timeline', draw_type, driver_name, vendor, dates)
                try:
                    for try_count in range(REPEAT - count):
                        print(f'Testing {vendor} {dates} {try_count}')
                        result = test_timeline(
                            driver,
                            f'http://localhost:8070/{vendor}',
                            dates, draw_type)
                        print('amount: {} api: {} total: {}'.format(*result))
                        conn.execute(
                            'insert into timeline'
                            '(drawtype, browser, vendor, dates, '
                            'amount, api, total) '
                            'values (?, ?, ?, ?, ?, ?, ?);',
                            (draw_type, driver_name, vendor, dates,
                             *result))
                        conn.commit()
                except Exception as e:
                    print(e)
                    try:
                        driver.close()
                    except Exception:
                        pass
                    finally:
                        driver = driver_cls()
                    continue
        finally:
            driver.close()


def main():
    try:
        mode = sys.argv[1]
    except IndexError:
        print('Usage: {} <vector|heatmap|timeline>'.format(sys.argv[0]),
              file=sys.stderr)
        exit(1)

    if mode in ('vector', 'heatmap',):
        normal_test(mode)
    elif mode == 'timeline':
        timeline_test('heatmap')

    out_to_csv()
    conn.close()


if __name__ == '__main__':
    main()
