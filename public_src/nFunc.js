class VendorModule extends TestModule {
    constructor(selector, coords, zoom) {
        super(selector, coords, zoom);
        this.markers = [];
        this.vectorSelector = `${this.selector} > div:nth-child(1) > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1)`;
        this.heatmapSelector = `${this.selector} > div:nth-child(1) > div > div:nth-child(1) > div:nth-child(2)`;
    }
    initMap() {
        let startTime = Date.now();
        this.map = new naver.maps.Map(document.querySelector(this.selector).id, {
            center: new naver.maps.LatLng(this.coords.lat, this.coords.lng),
            zoom: 19 - this.zoom,
        });
        naver.maps.Event.once(this.map, 'tilesloaded', () => {
            let elapsedTime = Date.now() - startTime;
            this.updateInitTime(elapsedTime);
        });
    }
    clearMap() {
        for (let marker of this.markers) {
            marker.setMap(null);
        }
        if (this.heatmap) {
            // setData([]) is not working.
            // It seams bug.
            /*
            this.heatmap.setData([]);
            this.heatmap.redraw();
            */

            this.heatmap.setMap(null);
            delete this.heatmap;
        }
        this.markers = [];
    }

    drawRows(rows, drawType) {
        let promise = new Promise((resolve, reject) => {
            let startTime = Date.now();
            let jiggleTime = 1000;
            let observer = new MutationObserver((mutations, instance) => {
                let totalTime = Date.now() - startTime;
                let callback = () => {
                    this.updateTotalCallTime(totalTime);
                    instance.disconnect();
                    resolve({
                        amount: rows.length,
                        apiCallTime,
                        totalTime,
                    });
                };
                for (let mutation of mutations) {
                    if (mutation.type === 'childList' && mutation.addedNodes.length) {
                        this.antiJiggle('total', callback, jiggleTime);
                    }
                }
            });
            switch(drawType) {
            case 'vector':
                jiggleTime = 1000;
                observer.observe(document.querySelector(this.vectorSelector), {
                    subtree: false,
                    childList: true,
                });
                this.drawVectorLayer(rows);
                break;
            case 'heatmap':
                jiggleTime = 500;
                observer.observe(document.querySelector(this.heatmapSelector), {
                    subtree: true,
                    childList: true,
                });
                this.drawHeatmapLayer(rows);
                break;
            default:
                console.error(`Unsupported draw type: ${drawType}`);
                return;
            }
            let apiCallTime = Date.now() - startTime;
            this.updateApiCallTime(apiCallTime);
        });
        return promise;
    }

    drawTimeline(json, drawType) {
        let times = [];
        // jshint ignore:start
        let drawAndSave = async (rows) => {
            this.clearMap();
            let time = await this.drawRows(rows, drawType);
            times.push(time);
        };
        // jshint ignore:end
        let promise = new Promise((resolve, reject) => resolve());

        for (let date in json) {
            let rows = json[date];
            promise = promise.then(drawAndSave.bind(this, rows));
        }

        return promise.then(() => times);
    }

    drawVectorLayer(rows) {
        for (let row of rows) {
            let opacity = row.amount * 0.00001 * 2;
            let circle = new naver.maps.Circle({
                map: this.map,
                center: new naver.maps.LatLng(row.x, row.y),
                radius: row.amount / 100,
                strokeColor: '#ff0000',
                strokeOpacity: opacity,
                strokeStyle: 'solid',
                fillColor: '#ff0000',
                fillOpacity: opacity,
            });

            this.markers.push(circle);
        }
    }

    drawHeatmapLayer(rows) {
        // FYI: https://navermaps.github.io/maps.js/docs/tutorial-Visualization.html
        // https://navermaps.github.io/maps.js/docs/naver.maps.visualization.HeatMap.html
        let data = rows.map(row =>
            new naver.maps.visualization.WeightedLocation(row.x, row.y, row.amount)
        );

        if (!this.heatmap) {
            this.heatmap = new naver.maps.visualization.HeatMap({
                map: this.map,
                data: data,
                radius: window.RADIUS || 50,
            });
        } else {
            this.heatmap.setData(data);
            this.heatmap.redraw();
        }

    }
}
