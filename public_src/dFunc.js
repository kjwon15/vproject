class VendorModule extends TestModule {
    constructor(selector, coords, zoom) {
        super(selector, coords, zoom);
        this.markers = [];
    }
    initMap() {
        let element = document.querySelector(this.selector);
        let options = {
            center: new daum.maps.LatLng(this.coords.lat, this.coords.lng),
            level: 20 - this.zoom,
        };
        let startTime = Date.now();
        this.map = new daum.maps.Map(element, options);
        daum.maps.event.addListener(this.map, 'tilesloaded', () => {
            let elapsedTime = Date.now() - startTime;
            this.updateInitTime(elapsedTime);
            // daum.maps.event.removeListener(this.map, 'idle', handler);
        });
    }

    clearMap() {
        for (let marker of this.markers) {
            marker.setMap(null);
        }
        this.markers = [];
    }
    drawRows(rows) {
        let startTime = Date.now();
        let observer = new MutationObserver((mutations, instance) => {
            let totalTime = Date.now() - startTime;
            let callback = () => {
                this.updateTotalCallTime(totalTime);
                instance.disconnect();
            };
            for (let mutation of mutations) {
                if (mutation.type === 'childList' && mutation.addedNodes.length) {
                    this.antiJiggle('total', callback, 5000);
                }
            }
        });
        observer.observe(document.querySelector(`${this.selector} > div:nth-child(1) > div > div:nth-child(5) > svg`), {
            subtree: false,
            childList: true,
        });
        for (let row of rows) {
            let opacity = row.amount * 0.00001 * 2;
            let circle = new daum.maps.Circle({
                map: this.map,
                center: new daum.maps.LatLng(row.x, row.y),
                radius: row.amount / 100,
                strokeColor: '#ff0000',
                strokeOpacity: opacity,
                strokeStyle: 'solid',
                fillColor: '#ff0000',
                fillOpacity: opacity,
            });

            this.markers.push(circle);
        }
        let apiCallTime = Date.now() - startTime;
        this.updateApiCallTime(apiCallTime);
    }
}
