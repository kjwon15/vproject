const ORIG_TITLE = document.title;
var module;

class TestModule {
    constructor(selector, coords, zoom){
        this.selector = selector;
        this.coords = coords;
        this.zoom = zoom;
        this.jigglers = {};
    }
    antiJiggle(type, callback, timeout=1000, args=null) {
        // This is utility function to use jiggling events.
        if (type in this.jigglers) {
            clearTimeout(this.jigglers[type]);
        }
        let handle;
        if (args !== null) {
            handle = setTimeout(() => {callback(...args);}, timeout);
        } else {
            handle = setTimeout(callback, timeout);
        }
        this.jigglers[type] = handle;
    }
    initMap() {}
    updateInitTime(time) {
        window.initTime = time;
        document.title = `${ORIG_TITLE}-init`;
        console.log(`Init time: ${time}`);
    }
    clearMap() {}
    drawRows(rows, drawType) {}
    drawTimeline(json, drawType) {}
    updateApiCallTime(time) {
        window.apiCallTime = time;
        document.title = `${ORIG_TITLE}-apicall`;
        console.log(`API calling: ${time}`);
    }
    updateTotalCallTime(time) {
        window.totalTime = time;
        document.title = `${ORIG_TITLE}-complete`;
        console.log(`Total time: ${time}`);
    }
}

function prefetch(count) {
    return fetch(`${location.protocol}//${location.host}/count/${count}`)
        .then(res => res.json())
        .then(json => {
            console.log(`Fetched ${json.rows.length}`);
            return json.rows;
        })
        .then(rows => {
            rows = rows.map((obj) => ({
                x: obj.wgs_x,
                y: obj.wgs_y,
                amount: obj.subway_on,
            }));
            document.title = `${ORIG_TITLE}-fetched`;
            window.prefetchedRows = rows;
            return rows;
        });
}

function testWithCount(count, drawType) {
    document.title = ORIG_TITLE;
    module.clearMap();
    if (window.prefetchedRows && window.prefetchedRows.length === count) {
        module.drawRows(window.prefetchedRows, drawType);
    } else {
        prefetch(count).then((rows) => {
            module.drawRows(rows, drawType);
        });
    }
}

function prefetchTimeline(dates) {
    return fetch(`${location.protocol}//${location.host}/datecount/${dates}`)
        .then(res => res.json())
        .then(json => {
            console.log(`Fetched ${Object.keys(json).length}`);
            return json;
        })
        .then(json => {
            for (let date in json) {
                json[date] = json[date].map(row => ({
                    x: row.wgs_x,
                    y: row.wgs_y,
                    amount: row.subway_on,
                }));
            }
            document.title = `${ORIG_TITLE}-fetched`;
            window.prefetchedTimeline = json;
            return json;
        });
}

function testTimeline(dates, drawType) {
    document.title = ORIG_TITLE;
    module.clearMap();
    let promise;
    if (window.prefetchedTimeline && Object.keys(window.prefetchedTimeline).length === dates) {
        promise = module.drawTimeline(window.prefetchedTimeline, drawType);
    } else {
        promise = prefetchTimeline(dates).then(json => {
            return module.drawTimeline(json, drawType);
        });
    }
    promise.then(times => {
        let reduced = times.reduce((a, b) => {
            let result = {};
            Object.keys(a).map(key => {
                result[key] = a[key] + b[key];
            });
            return result;
        });
        console.log(reduced);
        window.result = reduced;
        document.title = `${ORIG_TITLE}-timeline-complete`;
    });
}

$(function() {
    $('#count-button').click(() => {
        let count = parseInt($('#count-input').val());
        module.clearMap();
        fetch(`${location.protocol}//${location.host}/count/${count}`)
            .then(res => res.json())
            .then(json => {
                console.log(`Fetched ${json.rows.length}`);
                return json.rows;
            })
            .then(rows => {
                let buttons = $('#buttons');
                rows = rows.map((obj) => ({
                    x: obj.wgs_x,
                    y: obj.wgs_y,
                    amount: obj.subway_on,
                }));
                buttons.html(null);
                let button = $('<button>');
                button.text('Draw!!');
                button.click(() => {
                    module.clearMap();
                    module.drawRows(rows);
                });
                buttons.append(button);
            });
    });

    module = new VendorModule(
        '#map', {
            lat: 37.554648,
            lng: 126.972559
        }, 12);
    module.initMap();
    window.module = module;
});
