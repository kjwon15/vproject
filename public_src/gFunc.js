class VendorModule extends TestModule {
    constructor(selector, coords, zoom) {
        super(selector, coords, zoom);
        this.circles = [];
    }

    initMap() {
        let startTime = Date.now();
        this.map = new google.maps.Map(document.querySelector(this.selector), {
            center: {
                lat: this.coords.lat,
                lng: this.coords.lng,
            },
            zoom: this.zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        });
        this.map.addListener('tilesloaded', () => {
            let elapsedTime = Date.now() - startTime;
            this.updateInitTime(elapsedTime);
        });
    }

    clearMap() {
        if (this.heatmap !== undefined) {
            this.heatmap.setMap(null);
        }
        for (let marker of this.circles) {
            marker.setMap(null);
        }
        this.circles = [];
        if (this.heatmap) {
            this.heatmap.setData([]);
        }
    }

    drawRows(rows, drawType) {
        switch (drawType) {
        case 'vector':
            this.drawVectorRows(rows);
            break;
        case 'heatmap':
            this.drawHeatMapRows(rows);
            break;
        default:
            console.error(`Not supported draw type: ${drawType}.`);
        }
    }

    drawTimeline(json, drawType) {
        let drawFunction;
        switch(drawType) {
        case 'vector':
            drawFunction = this.drawVectorRows.bind(this);
            break;
        case 'heatmap':
            drawFunction = this.drawHeatMapRows.bind(this);
            break;
        }
        let promise = new Promise((resolve, reject) => resolve());
        let times = [];
        // jshint ignore:start
        // ES2017 feature
        let drawAndSave = async (rows) => {
            let time = await drawFunction(rows);
            times.push(time);
        };
        // jshint ignore:end
        for (let date in json) {
            let rows = json[date];
            promise = promise.then(drawAndSave.bind(this, rows));
        }

        return promise.then(() => times);
    }

    drawHeatMapRows(rows) {
        let promise = new Promise((resolve, reject) => {
            let startTime = Date.now();

            let data = rows.map((row) => {
                return {
                    location: new google.maps.LatLng(row.x, row.y),
                    weight: row.amount,
                };
            });

            let dataPreparedTime = Date.now();
            console.log(`Data preparing: ${dataPreparedTime - startTime}`);

            let observer = new MutationObserver((mutations, instance) => {
                let totalTime = Date.now() - startTime;
                let callback = () => {
                    this.updateTotalCallTime(totalTime);
                    instance.disconnect();
                    resolve({
                        amount: rows.length,
                        apiCallTime,
                        totalTime,
                    });
                };
                for (let mutation of mutations) {
                    if (mutation.type === 'childList' && mutation.addedNodes.length) {
                        this.antiJiggle('total', callback, 100);
                    }
                }
            });
            observer.observe(
                document.querySelector(`${this.selector} > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)`), {
                    subtree: true,
                    childList: true,
                });
            if (this.heatmap === undefined) {
                this.heatmap = new google.maps.visualization.HeatmapLayer({
                    data: data,
                    map: this.map,
                    radius: window.RADIUS || 50,
                });
            } else {
                this.heatmap.setData(data);
            }


            let apiCallTime = Date.now() - dataPreparedTime;
            this.updateApiCallTime(apiCallTime);
        });
        return promise;
    }

    drawVectorRows(rows) {
        let promise = new Promise((resolve, reject) => {
            this.clearMap();
            let startTime = Date.now();
            let observer = new MutationObserver((mutations, instance) => {
                let totalTime = Date.now() - startTime;
                let callback = () => {
                    this.updateTotalCallTime(totalTime);
                    instance.disconnect();
                    resolve({
                        amount: rows.length,
                        apiCallTime,
                        totalTime,
                    });
                };
                for (let mutation of mutations) {
                    if (mutation.type === 'childList' && mutation.addedNodes.length) {
                        this.antiJiggle('total', callback, 10000);
                    }
                }
            });
            observer.observe(
                document.querySelector(`${this.selector} > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)`), {
                    subtree: true,
                    childList: true,
                });
            for (let row of rows) {
                let opacity = row.amount * 0.00001 * 2;
                let circle = new google.maps.Circle({
                    center: new google.maps.LatLng(row.x, row.y),
                    radius: row.amount / 100,
                    strokeColor: '#FF0000',
                    strokeOpacity: opacity,
                    fillColor: '#FF0000',
                    fillOpacity: opacity
                });

                this.circles.push(circle);
                circle.setMap(this.map);
            }
            let apiCallTime = Date.now() - startTime;
            this.updateApiCallTime(apiCallTime);

        });
        return promise;
    }
}
