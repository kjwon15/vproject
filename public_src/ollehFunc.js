class VendorModule extends TestModule {
    constructor (selector, coords, zoom) {
        super(selector, coords, zoom);
        this.markers = [];
    }

    initMap() {
        let startTime = Date.now();
        let element = document.querySelector(this.selector);
        let options = {
            center: new olleh.maps.LatLng(this.coords.lat, this.coords.lng),
            zoom: 18 - this.zoom,
            mapTypeId: 'ROADMAP',
        };

        this.map = new olleh.maps.Map(element, options);
        window.map_ = this.map;
        let imgs = element.querySelectorAll('#layer_container > #root_pane > div:first-child > div:first-child > img');
        let handler = () => {
            let elapsedTime = Date.now() - startTime;
            this.antiJiggle('init', () => {
                this.updateInitTime(elapsedTime);
            });
        };
        for (let img of imgs) {
            img.addEventListener('load', handler);
        }
    }

    clearMap() {
        for (let marker of this.markers) {
            marker.setMap(null);
        }
        this.markers = [];
    }

    drawRows(rows) {
        let startTime = Date.now();
        let observer = new MutationObserver((mutations, instance) => {
            let totalTime = Date.now() - startTime;
            let callback = () => {
                this.updateTotalCallTime(totalTime);
                instance.disconnect();
            };
            for (let mutation of mutations) {
                if (mutation.type === 'childList' && mutation.addedNodes.length) {
                    this.antiJiggle('total', callback, 1000);
                }
            }
        });
        let element = document.querySelector(`${this.selector} > #layer_container > #root_pane > div:nth-child(8)`);
        element = element.querySelector('svg') || element;
        observer.observe(element, {
            subtree: false,
            childList: true,
        });
        for (let row of rows) {
            let opacity = row.amount * 0.00001 * 2;
            let circle = new olleh.maps.vector.Circle({
                center: new olleh.maps.LatLng(row.x, row.y),
                radius: row.amount / 100,
                strokeColor: '#FF0000',
                strokeOpacity: opacity,
                fillColor: '#FF0000',
                fillOpacity: opacity,
            });

            this.markers.push(circle);
            circle.setMap(this.map);
        }
        let apiCallTime = Date.now() - startTime;
        this.updateApiCallTime(apiCallTime);
    }
}
