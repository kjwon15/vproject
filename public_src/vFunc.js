class VendorModule extends TestModule {
    constructor(selector, coords, zoom) {
        super(selector, coords, zoom);
    }

    initMap() {
        let startTime = Date.now();
        vw.ol3.MapOptions = {
            basemapType: vw.ol3.BasemapType.GRAPHIC,
            controlDensity: vw.ol3.DensityType.FULL,
            interactionDensity: vw.ol3.DensityType.BASIC,
            controlsAutoArrange: true,
            homePosition: vw.ol3.CameraPosition,
            initPosition: vw.ol3.CameraPosition
        };

        this.source = new ol.source.Vector({
            projection: 'EPSG:4326',
        });

        this.map = new vw.ol3.Map(document.querySelector(this.selector).id, vw.ol3.MapOptions);
        this.map.getView().setCenter(ol.proj.transform([this.coords.lng, this.coords.lat], 'EPSG:4326', 'EPSG:3857'));
        this.map.getView().setZoom(this.zoom);

        this.map.on('postrender', (event) => {
            let elapsedTime = Date.now() - startTime;
            this.antiJiggle('init', () => {
                this.updateInitTime(elapsedTime);
                // XXX
                this.map.unByKey(event);
            });
        });
    }

    clearMap() {
        if (this.source) {
            for (let feature of this.source.getFeatures()) {
                this.source.removeFeature(feature);
            }
        }
        this.map.removeLayer(this.layer);
    }

    drawRows(rows, drawType) {
        let promise = new Promise((resolve, reject) => {
            let startTime = Date.now();
            let drawFunc;

            switch (drawType) {
            case 'vector':
                this.layer = new ol.layer.Vector({
                    source: this.source,
                    updateWhileAnimating: true,
                    style: this.vectorStyleFunction,
                });
                this.map.addLayer(this.layer);
                drawFunc = this.drawVectorLayer.bind(this);
                break;
            case 'heatmap':
                this.layer = new ol.layer.Heatmap({
                    source: this.source,
                    radius: window.RADIUS || 25,
                    blur: window.BLUR || 25,
                    gradient: ['#0f0', '#0f0', '#ff0', '#f00'],
                });
                this.map.addLayer(this.layer);
                drawFunc = this.drawHeatmapLayer.bind(this);
                break;
            default:
                console.error(`Not supported draw type: ${drawType}`);
                return;
            }

            let prepTime = Date.now();
            console.log(`prepare time: ${prepTime - startTime}`);

            drawFunc(rows);

            let apiCallTime = Date.now() - prepTime;
            this.updateApiCallTime(apiCallTime);
            this.map.renderSync();
            let totalTime = Date.now() - startTime;
            this.updateTotalCallTime(totalTime);


            setTimeout(() => {
                resolve({
                    amount: rows.length,
                    apiCallTime,
                    totalTime,
                });
            }, 100);
        });
        return promise;
    }

    drawTimeline(json, drawType) {
        let times = [];
        // jshint ignore:start
        let drawAndSave = async (rows) => {
            this.clearMap();
            let time = await this.drawRows(rows, drawType);
            times.push(time);
        };
        // jshint ignore:end

        let promise = new Promise((resolve, reject) => resolve());
        for (let date in json) {
            let rows = json[date];
            promise = promise.then(drawAndSave.bind(this, rows));
        }

        return promise.then(() => times);
    }

    drawVectorLayer(rows) {
        for (let row of rows) {
            let geom = ol.proj.transform([parseFloat(row.y), parseFloat(row.x)], 'EPSG:4326', 'EPSG:3857');
            let circle = new ol.geom.Circle(geom, row.amount / 100);
            let feature = new ol.Feature(circle);
            feature.set('amount', row.amount);

            this.source.addFeature(feature);
        }
    }

    drawHeatmapLayer(rows) {
        let maxAmount = Math.max(...rows.map(row => row.amount));
        for (let row of rows) {
            let coords = ol.proj.transform([parseFloat(row.y), parseFloat(row.x)], 'EPSG:4326', 'EPSG:3857');
            let geometry = new ol.geom.Point(coords);
            let feature = new ol.Feature({
                geometry: geometry,
                weight: row.amount / maxAmount,
            });
            this.source.addFeature(feature);
        }
    }

    vectorStyleFunction(feature, resolution) {
        let amount = feature.get('amount');
        let opacity = amount * 0.00001 * 2;
        return [
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: [255, 0, 0, opacity],
                    width: 0,
                }),
                fill: new ol.style.Fill({
                    color: [255, 0, 0, opacity],
                }),
            }),
        ];
    }
}
