import csv
import itertools
import re
import sqlite3
import sys
import time

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support import ui

WAIT_SECONDS = 30
VENDORS = ('v', 'g', 'o', 'n', 'd', 'k')
AMOUNTS = range(1000, 20000 + 1, 1000)
REPEAT = 5

BROWSERS = {
    'chrome': webdriver.Chrome,
    'firefox': webdriver.Firefox,
}

conn = sqlite3.connect('out.sqlite')
conn.execute(
    'create table if not exists memory('
    'browser, vendor, amount, init, before, after'
    ');'
)


def get_count(browser, vendor, amount):
    count = conn.execute(
        'select count(*) from memory where '
        'browser = ? and '
        'vendor = ? and '
        'amount = ?;',
        (browser, vendor, amount)
    ).fetchone()[0]
    return count


def out_to_csv():
    with open('mem.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator='\n')
        writer.writerow((
            'browser',
            'vendor',
            'amount',
            'init_mem',
            'before_mem',
            'after_mem',
        ))

        cur = conn.execute(
            'select browser, vendor, amount, init, before, after '
            'from memory '
            'order by browser, vendor, amount'
        )

        for row in cur:
            browser, vendor, amount, init, before, after = row
            init = init.replace(' MB', '')
            before = before.replace(' MB', '')
            after = after.replace(' MB', '')
            writer.writerow((browser, vendor, amount, init, before, after))


def get_mem_usage_firefox(driver):
    url = driver.current_url
    window_handle = driver.current_window_handle
    driver.execute_script('window.open()')
    driver.switch_to.window(driver.window_handles[-1])

    # TODO: Clear this messy code.
    driver.get('about:memory')
    driver.find_element_by_id('measureButton').click()
    for try_count in range(5):
        try:
            id_str = f':explicit/window-objects/top({url}, id={window_handle})'
            element = driver.find_element_by_xpath(f'//*[contains(@id, "{id_str}")]')
            result = element.find_element_by_class_name('mrValue').text
        except Exception as e:
            error = e
            time.sleep(.5)
            continue
        else:
            driver.close()
            driver.switch_to.window(window_handle)
            break
    else:
        print(str(error))
    return result


def get_mem_usage_chrome(driver):
    title = driver.title
    pattern = re.compile(
        rf'Tab \[(?:.*\|)?{title}(?:\|.*)?\] (?P<private>.*?) private,',
        re.MULTILINE)
    window_handle = driver.current_window_handle
    driver.execute_script('window.open()')
    driver.switch_to.window(driver.window_handles[-1])

    driver.get('chrome://system')
    driver.find_element_by_id('mem_usage-value-btn').click()
    summary = driver.find_element_by_id('mem_usage-value').text
    matched = pattern.search(summary)
    if not matched:
        print('XXXX!!')
        print(summary)
    driver.close()
    driver.switch_to.window(window_handle)
    return matched.group('private')


def get_mem_usage(driver):
    if isinstance(driver, webdriver.Firefox):
        return get_mem_usage_firefox(driver)
    elif isinstance(driver, webdriver.Chrome):
        return get_mem_usage_chrome(driver)


def test_with_count(driver_cls, vendor, count):
    URL = f'http://localhost:8070/{vendor}'

    try:
        driver = driver_cls()

        wait = ui.WebDriverWait(driver, WAIT_SECONDS)
        driver.get(URL)

        wait.until(EC.title_contains('-init'))

        # Time to GC
        time.sleep(1)

        init_mem = get_mem_usage(driver)

        # Fetch and measure for analyse correctly.
        driver.execute_script(
            f'prefetch({count})'
        )

        wait.until(EC.title_contains('-fetched'))
        time.sleep(1)

        before_mem = get_mem_usage(driver)

        driver.execute_script(
            f'testWithCount({count}, false)'
        )

        wait.until(EC.title_contains('-complete'))
        time.sleep(1)

        after_mem = get_mem_usage(driver)
    except Exception as e:
        print(str(e))
        return
    else:
        return (init_mem, before_mem, after_mem)
    finally:
        driver.close()


if __name__ == '__main__':
    for amount, browser, vendor in itertools.product(AMOUNTS, BROWSERS.keys(), VENDORS):
        count = get_count(browser, vendor, amount)
        for try_count in range(count, REPEAT):
            print(f'Testing {browser}-{vendor}-{amount} ({try_count})')
            result = test_with_count(BROWSERS[browser], vendor, amount)
            if not result:
                print('Failed')
                continue

            print(result)
            conn.execute(
                'insert into memory'
                '(browser, vendor, amount, init, before, after) '
                'values(?, ?, ?, ?, ?, ?)',
                (browser, vendor, amount, *result)
            )
            conn.commit()

    out_to_csv()
    conn.close()
